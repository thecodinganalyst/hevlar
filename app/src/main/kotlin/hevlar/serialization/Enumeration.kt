package hevlar.serialization

import kotlinx.serialization.Serializable

@Serializable
data class Enumeration(
    val name: String,
    val kind: String,
    val values: List<String>
)
