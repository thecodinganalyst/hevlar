package hevlar.serialization

import kotlinx.serialization.Serializable

@Serializable
data class Definition(val name: String, val kind: String) {
    var required: Boolean = false

    constructor(name: String, kind: String, required: Boolean): this(name, kind){
        this.required = required
    }

}