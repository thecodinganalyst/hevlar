package hevlar.serialization

import kotlinx.serialization.Serializable

@Serializable
data class ModelEnum(
    val name: String,
    val kind: String,
    val definitions: Collection<Definition>,
    val values: List<Map<String, String>>
)
