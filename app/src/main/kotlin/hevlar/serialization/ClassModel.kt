package hevlar.serialization

import kotlinx.serialization.Serializable

@Serializable
data class ClassModel(
    val name: String,
    val kind: String,
    val definitions: Collection<Definition>,
    val keys: Collection<String>
)