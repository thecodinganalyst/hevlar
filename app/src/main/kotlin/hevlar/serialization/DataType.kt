package hevlar.serialization

import com.charleskorn.kaml.Yaml
import kotlinx.serialization.decodeFromString
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeParseException

interface DataCheck {
    fun check(value: String): Boolean
}

sealed class DataType(val typeName: String, val parent: DataType?): DataCheck {
    override fun check(value: String) = true
    companion object {
        private fun allTypes(): List<DataType> {
            return listOf(
                TextType,
                BooleanType,
                NumberType,
                DateTimeType,
                DateType,
                DefinitionType,
                ClassModelType,
                EnumType,
                ModelEnumType
            )
        }

        fun isValidType(typeName: String): Boolean {
            return allTypes().map { it.typeName }.contains(typeName)
        }

        fun getDataType(typeName: String): DataType? {
            return allTypes().firstOrNull { it.typeName.equals(typeName, true) }
        }
    }

}

object TextType: DataType("Text", null){
    override fun check(value: String): Boolean {
        return true
    }
}

object BooleanType: DataType("Boolean", TextType) {
    override fun check(value: String): Boolean {
        return value.equals("Yes", true) ||
                value.equals("No", true) ||
                value.equals("True", true) ||
                value.equals("False", true) ||
                value.equals("1", true) ||
                value.equals("0", true)
    }
}

object NumberType: DataType("Number", TextType){
    override fun check(value: String): Boolean {
        return value.toDoubleOrNull() != null
    }
}

object DateTimeType: DataType("DateTime", TextType){
    override fun check(value: String): Boolean {
        try {
            LocalDateTime.parse(value)
        }catch (exception: DateTimeParseException){
            return false
        }
        return true
    }
}

object DateType: DataType("Date", DateTimeType){
    override fun check(value: String): Boolean {
        try {
            LocalDate.parse(value)
        }catch (exception: DateTimeParseException){
            return false
        }
        return true
    }
}

object DefinitionType: DataType("Definition", TextType) {
    override fun check(value: String): Boolean {
        return try{
            val definition = Yaml.default.decodeFromString<Definition>(value)
            definition.name.isNotEmpty()
        }catch (exception: java.lang.Exception){
            false
        }
    }
}

object ClassModelType: DataType("ClassModel", DefinitionType) {
    override fun check(value: String): Boolean {
        return try{
            val classModel = Yaml.default.decodeFromString<ClassModel>(value)
            super.check(value) && classModel.keys.isNotEmpty() && classModel.definitions.isNotEmpty() && classModel.kind.equals("ClassModel", true)
        }catch (exception: java.lang.Exception){
            false
        }
    }
}

object EnumType: DataType("Enum", DefinitionType) {
    override fun check(value: String): Boolean {
        return try{
            val enumeration = Yaml.default.decodeFromString<Enumeration>(value)
            super.check(value) && enumeration.kind.equals("Enum", true)
        }catch (exception: java.lang.Exception){
            false
        }
    }
}

object ModelEnumType: DataType("ModelEnum", DefinitionType) {
    override fun check(value: String): Boolean {
        return try{
            val modelEnum = Yaml.default.decodeFromString<ModelEnum>(value)
            val defKeys_mandatory = modelEnum.definitions.filter { it.required }.map { it.name }
            val defKeys_all = modelEnum.definitions.map { it.name }
            val valuesKeys = modelEnum.values.flatMap { it.keys }.toHashSet()
            val allMandatoryKeysPresent = valuesKeys.containsAll(defKeys_mandatory)
            val noUnknownKeys = valuesKeys.minus(defKeys_all).isEmpty()
            super.check(value) && modelEnum.kind.equals("ModelEnum", true) && allMandatoryKeysPresent && noUnknownKeys
        }catch (exception: java.lang.Exception){
            false
        }
    }
}