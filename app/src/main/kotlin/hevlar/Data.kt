package hevlar

import hevlar.serialization.Definition
import kotlinx.serialization.Serializable

@Serializable
data class Data(val valueMap: Map<String, String>){
    var definition: Definition? = null
    var name: String? = null

    constructor(value: String): this(mapOf(Pair("name", value)))
}