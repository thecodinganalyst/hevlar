package hevlar.model

class ClassModel(override val name: String, val definitions: Collection<Definition>, val keys: Collection<String>) : Model {
    companion object {
        fun fromSerialization(classModel: hevlar.serialization.ClassModel): ClassModel {
            return ClassModel(
                classModel.name,
                Definition.fromSerializationCollection(classModel.definitions),
                classModel.keys)
        }
    }
}