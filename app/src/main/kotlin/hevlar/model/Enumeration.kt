package hevlar.model

class Enumeration(override val name: String, values: List<Any>) : Model {

    var definitions: Collection<Definition>? = null

    constructor(name: String, values: List<Map<String, String>>, definitions: Collection<Definition>): this(name, values){
        this.definitions = definitions
    }

    companion object {
        fun fromSerialization(enumeration: hevlar.serialization.Enumeration): Enumeration {
            return Enumeration(enumeration.name, enumeration.values)
        }

        fun fromSerialization(enumeration: hevlar.serialization.ModelEnum): Enumeration {
            return Enumeration(
                enumeration.name,
                enumeration.values,
                Definition.fromSerializationCollection(enumeration.definitions))
        }
    }
}