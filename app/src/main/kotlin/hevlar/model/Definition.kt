package hevlar.model

class Definition(val name: String, val kind: String) {
    var required = false

    constructor(name: String, kind: String, required: Boolean): this(name, kind){
        this.required = required
    }

    companion object {
        fun fromSerialization(definition: hevlar.serialization.Definition): Definition {
            return Definition(definition.name, definition.kind, definition.required)
        }

        fun fromSerializationCollection(definitions: Collection<hevlar.serialization.Definition>): Collection<Definition>{
            return definitions.map { fromSerialization(it) }
        }
    }
}