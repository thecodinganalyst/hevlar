package hevlar

import com.charleskorn.kaml.Yaml
import com.charleskorn.kaml.yamlMap
import hevlar.model.Model
import hevlar.serialization.*
import kotlinx.serialization.decodeFromString
import java.io.File

class System {

    private var modelMap: Map<String, Model>? = null

    init {
        val systemFolders = getSystemFolders()
        val modelFolder = systemFolders?.firstOrNull { it.name == "model" }
        modelMap = modelFolder?.let { readSystemModel(it) }
    }

    private fun readSystemModel(modelFolder: File): Map<String, Model>{
        val classModels = modelFolder.listFiles()
            ?.map { it.readText() }
            ?.filter { ClassModelType == getKindOfYaml(it) }
            ?.map { readYaml<ClassModel>(it) }
            ?.map { hevlar.model.ClassModel.fromSerialization(it) }

        val enums = modelFolder.listFiles()
            ?.map { it.readText() }
            ?.filter { EnumType == getKindOfYaml(it) }
            ?.map { readYaml<Enumeration>(it) }
            ?.map { hevlar.model.Enumeration.fromSerialization(it) }

        val modelEnums = modelFolder.listFiles()
            ?.map { it.readText() }
            ?.filter { ModelEnumType == getKindOfYaml(it) }
            ?.map { readYaml<ModelEnum>(it) }
            ?.map { hevlar.model.Enumeration.fromSerialization(it) }

        return mutableMapOf(
            *classModels.orEmpty().map { Pair(it.name, it) }.toTypedArray(),
            *enums.orEmpty().map { Pair(it.name, it) }.toTypedArray(),
            *modelEnums.orEmpty().map { Pair(it.name, it) }.toTypedArray()
        )
    }

    private fun getKindOfYaml(yamlString: String): DataType? {
        return Yaml.default.parseToYamlNode(yamlString)
            .yamlMap
            .getScalar("kind")
            ?.content
            ?.let { DataType.getDataType(it) }
    }

    private inline fun <reified T> readYaml(yamlString: String): T {
        return Yaml.default.decodeFromString(yamlString)
    }

    private fun getSystemFolders(): Array<File>? {
        val systemFolderURL = this::class.java.getResource("/system")
        return systemFolderURL?.let {
            File(systemFolderURL.toURI()).listFiles {
                    file -> file.isDirectory
            }
        }
    }

}