package hevlar.serialization

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import kotlin.test.*

internal class NumberTypeTest {

    @Test
    fun check() {
        assertThat(NumberType.check("123"), equalTo(true))
        assertThat(NumberType.check("12.334"), equalTo(true))
        assertThat(NumberType.check("0.123"), equalTo(true))
        assertThat(NumberType.check("-12"), equalTo(true))
        assertThat(NumberType.check("-12.7"), equalTo(true))
        assertThat(NumberType.check("10000000000"), equalTo(true))
        assertThat(NumberType.check("one hundred"), equalTo(false))
    }
}