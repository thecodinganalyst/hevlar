package hevlar.serialization

import org.hamcrest.Matchers.*
import org.hamcrest.MatcherAssert.assertThat
import kotlin.test.*

internal class TextTypeTest {
    @Test
    fun check() {
        assertThat(TextType.check("hello"), equalTo(true))
    }
}