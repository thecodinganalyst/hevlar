package hevlar.serialization

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import kotlin.test.*

internal class DateTypeTest {

    @Test
    fun check() {
        assertThat(DateType.check("2020-01-01"), equalTo(true))
        assertThat(DateType.check("01-01-2020"), equalTo(false))
        assertThat(DateType.check("12-Jan-2020"), equalTo(false))
        assertThat(DateType.check("something"), equalTo(false))
    }
}