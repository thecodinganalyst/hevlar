package hevlar.serialization

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import kotlin.test.*

internal class DefinitionTypeTest {

    @Test
    fun check_Valid() {
        var value = """
            name: something
            kind: Text
        """.trimIndent()

        assertThat(DefinitionType.check(value), equalTo(true))

        value = """
            name: something
            kind: Boolean
        """.trimIndent()

        assertThat(DefinitionType.check(value), equalTo(true))

        value = """
            name: something
            kind: Number
        """.trimIndent()

        assertThat(DefinitionType.check(value), equalTo(true))

        value = """
            name: something
            kind: DateTime
        """.trimIndent()

        assertThat(DefinitionType.check(value), equalTo(true))

        value = """
            name: something
            kind: Date
        """.trimIndent()

        assertThat(DefinitionType.check(value), equalTo(true))

        value = """
            name: something
            kind: Definition
        """.trimIndent()

        assertThat(DefinitionType.check(value), equalTo(true))

        value = """
            name: something
            kind: Model
        """.trimIndent()

        assertThat(DefinitionType.check(value), equalTo(true))
    }

    fun check_Invalid() {
        val value = """
            name: something
            kind: somethingelse
        """.trimIndent()

        assertThat(DefinitionType.check(value), equalTo(false))
    }

    @Test
    fun check_Valid_Required() {
        var value = """
            name: something
            kind: Text
            required: True
        """.trimIndent()
        assertThat(DefinitionType.check(value), equalTo(true))

        value = """
            name: something
            kind: Text
            required: False
        """.trimIndent()
        assertThat(DefinitionType.check(value), equalTo(true))

        value = """
            name: something
            kind: Text
            required: Text
        """.trimIndent()
        assertThat(DefinitionType.check(value), equalTo(false))
    }
}