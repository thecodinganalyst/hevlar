package hevlar.serialization

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import kotlin.test.*

internal class BooleanTypeTest {

    @Test
    fun check() {
        assertThat(BooleanType.check("true"), equalTo(true))
        assertThat(BooleanType.check("false"), equalTo(true))
        assertThat(BooleanType.check("True"), equalTo(true))
        assertThat(BooleanType.check("False"), equalTo(true))
        assertThat(BooleanType.check("TRUE"), equalTo(true))
        assertThat(BooleanType.check("FALSE"), equalTo(true))

        assertThat(BooleanType.check("yes"), equalTo(true))
        assertThat(BooleanType.check("no"), equalTo(true))
        assertThat(BooleanType.check("Yes"), equalTo(true))
        assertThat(BooleanType.check("No"), equalTo(true))
        assertThat(BooleanType.check("YES"), equalTo(true))
        assertThat(BooleanType.check("NO"), equalTo(true))

        assertThat(BooleanType.check("1"), equalTo(true))
        assertThat(BooleanType.check("0"), equalTo(true))

        assertThat(BooleanType.check("2"), equalTo(false))
        assertThat(BooleanType.check("something"), equalTo(false))
    }
}