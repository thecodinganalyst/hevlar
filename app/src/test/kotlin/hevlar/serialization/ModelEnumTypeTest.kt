package hevlar.serialization

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import kotlin.test.*

internal class ModelEnumTypeTest {

    @Test
    fun check() {
        val input = """
                    name: account_group
                    kind: modelEnum
                    definitions:
                      - name: name
                        kind: text
                      - name: account_type
                        kind: account_type
                    values:
                      - name: Fixed Assets
                        account_type: debit
                      - name: Current Assets
                        account_type: debit
                      - name: Current Liabilities
                        account_type: credit
                      - name: Long Term Liabilities
                        account_type: credit
                      - name: Expenses
                        account_type: debit
                      - name: Revenue
                        account_type: credit
                      - name: Gains
                        account_type: credit
                      - name: Losses
                        account_type: debit
                """.trimIndent()

        assertThat(ModelEnumType.check(input), equalTo(true))
    }

    @Test
    fun check_valuesDontMatchDefinitionsShouldFail() {
        val input = """
                    name: account_group
                    kind: modelEnum
                    definitions:
                      - name: name
                        kind: text
                      - name: account_type
                        kind: account_type
                    values:
                      - name: Fixed Assets
                        account: debit
                      - name: Current Assets
                        account: debit
                      - name: Current Liabilities
                        account: credit
                """.trimIndent()

        assertThat(ModelEnumType.check(input), equalTo(false))
    }

    @Test
    fun check_valuesWithUnknownKeysShouldFail() {
        val input = """
                    name: account_group
                    kind: modelEnum
                    definitions:
                      - name: name
                        kind: text
                      - name: account_type
                        kind: account_type
                    values:
                      - name: Fixed Assets
                        account_type: debit
                        account: debit
                      - name: Current Assets
                        account_type: debit
                        account: debit
                      - name: Current Liabilities
                        account_type: debit
                        account: credit
                """.trimIndent()

        assertThat(ModelEnumType.check(input), equalTo(false))
    }

    @Test
    fun check_valuesWithoutOptionalDefinitionFieldsShouldPass() {
        val input = """
                    name: account_group
                    kind: modelEnum
                    definitions:
                      - name: name
                        kind: text
                      - name: account_type
                        kind: account_type
                        required: false
                    values:
                      - name: Fixed Assets
                      - name: Current Assets
                      - name: Current Liabilities
                """.trimIndent()

        assertThat(ModelEnumType.check(input), equalTo(true))
    }

}