package hevlar.serialization

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import kotlin.test.*

internal class ClassModelTypeTest {

    @Test
    fun check() {
        val input = """
                    name: account
                    kind: classModel
                    definitions:
                      - name: id
                        kind: text
                      - name: name
                        kind: text
                        required: true
                    keys: 
                      - id
                """.trimIndent()

        assertThat(ClassModelType.check(input), equalTo(true))
    }

    @Test
    fun check_withoutKeysShouldFail() {
        val input = """
                    name: account
                    kind: classModel
                    definitions:
                      - name: id
                        kind: text
                      - name: name
                        kind: text
                        required: true
                """.trimIndent()

        assertThat(ClassModelType.check(input), equalTo(false))
    }

    @Test
    fun check_withoutDefinitionsShouldFail() {
        val input = """
                    name: account
                    kind: classModel
                    keys: 
                      - id
                """.trimIndent()

        assertThat(ClassModelType.check(input), equalTo(false))
    }

    @Test
    fun check_withoutKindShouldFail() {
        val input = """
                    name: account
                    definitions:
                      - name: id
                        kind: text
                      - name: name
                        kind: text
                        required: true
                    keys: 
                      - id
                """.trimIndent()

        assertThat(ClassModelType.check(input), equalTo(false))
    }

    @Test
    fun check_withoutNameShouldFail() {
        val input = """
                    kind: classModel
                    definitions:
                      - name: id
                        kind: text
                      - name: name
                        kind: text
                        required: true
                    keys: 
                      - id
                """.trimIndent()

        assertThat(ClassModelType.check(input), equalTo(false))
    }

    @Test
    fun check_withNoItemsInDefinitionShouldFail() {
        val input = """
                    name: account
                    kind: classModel
                    definitions:
                    keys: 
                      - id
                """.trimIndent()

        assertThat(ClassModelType.check(input), equalTo(false))
    }
}