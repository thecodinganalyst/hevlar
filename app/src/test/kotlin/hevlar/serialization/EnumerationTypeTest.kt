package hevlar.serialization

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import kotlin.test.*

internal class EnumerationTypeTest {

    @Test
    fun check() {
        val input = """
                    name: currency
                    kind: enum
                    values: 
                      - SGD
                      - MYR
                      - USD
                      - EUR
                """.trimIndent()

        assertThat(EnumType.check(input), equalTo(true))
    }

    @Test
    fun check_withInvalidKindShouldFail() {
        val input = """
                    name: currency
                    kind: somethingelse
                    values: 
                      - SGD
                      - MYR
                      - USD
                      - EUR
                """.trimIndent()

        assertThat(EnumType.check(input), equalTo(false))
    }

    @Test
    fun check_withoutValuesShouldFail() {
        val input = """
                    name: currency
                    kind: enum
                """.trimIndent()

        assertThat(EnumType.check(input), equalTo(false))
    }

    @Test
    fun check_withNoItemsInValueShouldFail() {
        val input = """
                    name: currency
                    kind: enum
                    values: 
                """.trimIndent()

        assertThat(EnumType.check(input), equalTo(false))
    }
}