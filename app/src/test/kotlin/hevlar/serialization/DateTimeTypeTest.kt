package hevlar.serialization

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import kotlin.test.*

internal class DateTimeTypeTest {

    @Test
    fun check() {
        assertThat(DateTimeType.check("2020-01-12T13:45:00"), equalTo(true))
        assertThat(DateTimeType.check("2020-01-12 13:45:00"), equalTo(false))
        assertThat(DateTimeType.check("anything else"), equalTo(false))
    }
}